<?php

namespace Ostendis\Utilities\helpers;

use DateTime;
use DateTimeZone;
use Ostendis\Utilities\models\enum\MsSqlData;
use Yii;

/**
 * Class DateTimeHelper
 *
 * @package   Ostendis\Utilities\helpers
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class DateTimeHelper
{
    public static $DATE_FORMATS = [
        MsSqlData::FORMAT_DATETIME2,
        MsSqlData::FORMAT_DATETIME,
        MsSqlData::FORMAT_SMALLDATETIME,
        MsSqlData::FORMAT_DATE,
        MsSqlData::FORMAT_TIME,
        MsSqlData::FORMAT_SMALLTIME,
    ];

    /**
     * Returns a DateTime object from a given unix timestamp
     *
     * @param int $timestamp
     * @return \DateTime
     * @throws \Exception
     */
    public static function fromUnixTimestamp(int $timestamp): DateTime
    {
        $gmt = new DateTime("@$timestamp");
        $tz = new DateTimeZone(Yii::$app->timeZone);
        $gmt->setTimezone($tz);
        return $gmt;
    }


    /**
     * Returns a DateTime object from a given sql date
     *
     * @param string $datetime
     * @return \DateTime|null
     */
    public static function fromDatabase(string $datetime): ?DateTime
    {
        $tz = new DateTimeZone(Yii::$app->timeZone);

        foreach (self::$DATE_FORMATS as $format) {
            /** @var \DateTime $converted */
            $converted = DateTime::createFromFormat($format, $datetime, $tz);
            if ($converted instanceof DateTime) {
                return $converted;
            }
        }
        return null;
    }


    /**
     * Returns a UTC string from a given string or DateTime object
     *
     * @param \DateTime|string $datetime
     * @return string
     */
    public static function toUtcString($datetime): string
    {
        if (!($datetime instanceof DateTime)) {
            $datetime = static::fromDatabase($datetime);
        }

        $tz = new DateTimeZone('UTC');
        $datetime->setTimezone($tz);

        return $datetime->format(DateTime::RFC3339_EXTENDED);
    }

    /**
     * Check if string is UTC date conform. Matches RFC3339_EXTENDED format
     *
     * @param string $utc
     * @return bool
     */
    public static function isUtcString(string $utc)
    {
        $matches = preg_match('/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}\+\d{2}:\d{2}/', $utc);
        return $matches === 1;
    }

    /**
     * Convert value to given DB format
     *
     * @param DateTime|string|int $value
     * @param string              $format
     * @return string
     */
    public static function toDbFormat($value, string $format): string
    {
        $fromFormat = 'Y-m-d\TH:i:s.uP';//DateTime::RFC3339_EXTENDED;
        $tz = new DateTimeZone(Yii::$app->timeZone);
        $dt = $value;

        if (is_int($value)) {
            $dt = DateTime::createFromFormat('U', $value);
        } else if (static::isUtcString($value)) {
            $dt = DateTime::createFromFormat($fromFormat, $value);
        }

        if ($dt instanceof DateTime) {
            $dt->setTimezone($tz);
            return $dt->format($format);
        }

        return $value;
    }
}
