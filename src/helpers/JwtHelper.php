<?php

namespace Ostendis\Utilities\helpers;

use Lcobucci\JWT\Builder as JWTBuilder;
use Lcobucci\JWT\Signer\Hmac\Sha512;
use Lcobucci\JWT\Token as JWTToken;
use Yii;

/**
 * Class JwtHelper
 *
 * @package   Ostendis\Utilities\helpers
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class JwtHelper
{

    /**
     * Generates token with standard settings
     *
     * @param array $config
     * @return JWTBuilder A token builder to work with
     */
    public static function generateToken(array $config = []): JWTBuilder
    {
        $issued = isset($config['issued']) ? $config['issued'] : time();
        $notBefore = isset($config['notBefore']) ? $config['notBefore'] : time();
        $expiration = isset($config['expiration']) ? $config['expiration'] : time();

        /** @var \sizeg\jwt\Jwt $jwt */
        $jwt = Yii::$app->jwt;

        $tokenBuilder = $jwt->getBuilder()
            ->setIssuer(Yii::$app->urlManager->createAbsoluteUrl('/'))// Configures the issuer (iss claim)
            ->setIssuedAt($issued)// Configures the time that the token was issue (iat claim)
            ->setNotBefore($notBefore)// Configures the time before which the token cannot be accepted (nbf claim)
            ->setExpiration($expiration)    // Configures the expiration time of the token (exp claim)
        ;

        return $tokenBuilder;
    }

    /**
     * Set payload for the token
     *
     * @param JWTBuilder $tokenbuilder
     * @param array      $payload
     */
    public static function setPayload(JWTBuilder $tokenbuilder, array $payload): void
    {
        if (count($payload) === 0) {
            return;
        }

        foreach ($payload as $key => $value) {
            $tokenbuilder->set($key, $value);
        }
    }

    /**
     * Get signed token
     *
     * @param JWTBuilder $tokenbuilder
     * @return JWTToken
     */
    public static function getSignedToken(JWTBuilder $tokenbuilder): JWTToken
    {
        /** @var \sizeg\jwt\Jwt $jwt */
        $jwt = Yii::$app->jwt;
        $signer = new Sha512();

        return $tokenbuilder
            ->sign($signer, $jwt->key)
            ->getToken();
    }

    /**
     * Verify token
     *
     * @param JWTToken|string $token
     * @return bool
     * @throws \Throwable
     */
    public static function verify($token): bool
    {
        /** @var \sizeg\jwt\Jwt $jwt */
        $jwt = Yii::$app->jwt;
        if (!($token instanceof JWTToken)) {
            $token = $jwt->getParser()->parse($token);
        }

        return $jwt->verifyToken($token);
    }

    /**
     * Get ID by token
     *
     * @param string $tokenStr
     * @return int
     */
    public static function getId(string $tokenStr): int
    {
        /** @var \sizeg\jwt\Jwt $jwt */
        $jwt = Yii::$app->jwt;

        $token = $jwt->getParser()->parse($tokenStr);
        return $token->getClaim('id');
    }
}
