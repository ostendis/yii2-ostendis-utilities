<?php

namespace Ostendis\Utilities\helpers;

use Ostendis\Utilities\models\enum\MsSqlData;

/**
 * Class TypeHelper
 *
 * @package   Ostendis\Utilities\helpers
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class TypeHelper
{
    const PRIMITIVES = [
        'bool',
        'boolean',
        'int',
        'integer',
        'float',
        'double',
        'string',
    ];

    /**
     * Determines if a given value is a primitive type
     *
     * @param mixed $value
     * @return bool
     */
    public static function isValuePrimitive($value): bool
    {
        return is_scalar($value);
    }

    /**
     * Cast a value into another given type
     *
     * @param null|string $toType
     * @param mixed       $value
     * @param bool        $skipNullValue
     * @return bool|float|int|string
     */
    public static function cast(?string $toType, $value, bool $skipNullValue = true)
    {
        if ($toType === null) return $value;
        if ($skipNullValue && $value === null) return $value;

        if (self::isTypePrimitive($toType)) {
            return self::castToPrimitive($toType, $value);
        } else if ($toType === 'UTCstring' || $toType === 'UTCstring|string') {
            return DateTimeHelper::toUtcString($value);
        } else if ($toType === "\DateTime" || $toType === 'DateTime') {
            return DateTimeHelper::fromDatabase($value);
        } else if ($toType === 'MsSqlDateTime2') {
            return DateTimeHelper::toDbFormat($value, MsSqlData::FORMAT_DATETIME2);
        } else if ($toType === 'MsSqlDateTime') {
            return DateTimeHelper::toDbFormat($value, MsSqlData::FORMAT_DATETIME);
        } else if ($toType === 'MsSqlSmallDateTime') {
            return DateTimeHelper::toDbFormat($value, MsSqlData::FORMAT_SMALLDATETIME);
        } else if ($toType === 'MsSqlDate') {
            return DateTimeHelper::toDbFormat($value, MsSqlData::FORMAT_DATE);
        } else if ($toType === 'MsSqlTime') {
            return DateTimeHelper::toDbFormat($value, MsSqlData::FORMAT_TIME);
        }

        return $value;
    }

    /**
     * Determines if a given type name is a primitive type
     *
     * @param string $type
     * @return bool
     */
    public static function isTypePrimitive(string $type): bool
    {
        return in_array($type, self::PRIMITIVES);
    }

    /**
     * Cast a primitive value into another given primitive type
     *
     * @param null|string $toType
     * @param mixed       $value
     * @return bool|float|int|string
     */
    public static function castToPrimitive(string $toType, $value)
    {
        switch ($toType) {
            case 'bool':
            case 'boolean':
                $value = boolval($value);
                break;
            case 'int':
            case 'integer':
                $value = intval($value);
                break;
            case 'float':
            case 'double':
                $value = floatval($value);
                break;
            default:
                $value = (string)$value;
                break;
        }

        return $value;
    }
}
