<?php

namespace Ostendis\Utilities\helpers;

/**
 * Class ArrayHelper
 *
 * @package   Ostendis\Utilities\helpers
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class ArrayHelper extends \yii\helpers\ArrayHelper
{
    /**
     * Flattens a given nested array
     *
     * @param array $subject
     * @param array $return
     * @return array
     */
    public static function flatten(array $subject, array $return = []): array
    {
        foreach ($subject as $key => $value) {
            if (is_array($subject[$key])) {
                $return = self::flatten($value, $return);
            } else {
                $return[$key] = $value;
            }
        }

        return $return;
    }

    /**
     * Inverts the keys and values of an array
     *
     * @param array $subject
     * @return array
     */
    public static function invertKeyValue(array $subject): array
    {
        if (!count($subject)) return [];

        $invertedArray = [];
        foreach ($subject as $key => $value) {

            if (is_array($value)) {
                $invertedArray[] = $key;
            } else {
                $invertedArray[$value] = $key;
            }
        }
        return $invertedArray;
    }

    /**
     * Move an array value from one position to another
     *
     * @param array          $subject
     * @param string|integer $fromKey
     * @param string|integer $toKey
     * @return bool
     */
    public static function moveValue(array &$subject, $fromKey, $toKey = null): bool
    {
        if (key_exists($fromKey, $subject)) {
            if ($toKey === null) {
                $subject[] = $subject[$fromKey];
            } else {
                $subject[$toKey] = $subject[$fromKey];
            }
            unset($subject[$fromKey]);

            return true;
        }

        return false;
    }

    /**
     * Cut an element from a given array
     *
     * @param array          $subject
     * @param string|integer $key
     * @return mixed
     */
    public static function cutValue(array &$subject, $key)
    {
        if (count($subject) === 0 || !key_exists($key, $subject)) {
            return null;
        }

        $value = $subject[$key];
        unset($subject[$key]);

        return $value;
    }
}
