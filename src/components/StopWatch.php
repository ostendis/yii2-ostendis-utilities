<?php

namespace Ostendis\Utilities\components;

use Yii;
use yii\base\Component;

/**
 * Class StopWatch
 *
 * @package   Ostendis\Utilities\components
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class StopWatch extends Component
{
    /** @var array */
    protected $watches = [];
    protected $running = 0;

    /**
     * Start a stopwatch with the given name
     *
     * @param string $name
     * @param bool   $log
     * @param array  $data
     * @return StopWatch
     */
    public function start(string $name, bool $log = true, array $data = []): StopWatch
    {
        $this->watches[$name]['start'] = microtime(true);
        $this->running++;

        if ($log) {
            $dataStr = count($data) ? ' | Data: (' . print_r($data, 1) . ')' : '';
            Yii::debug(sprintf('STOPWATCH: %s `%s` started%s', str_repeat('-', $this->running), $name, $dataStr), 'profiling');
        }
        return $this;
    }

    /**
     * Stop stopwatch with the given name and return duration time
     *
     * @param string $name
     * @param bool   $log
     * @param array  $data
     * @return StopWatch|null
     */
    public function stop(string $name, bool $log = true, array $data = []): ?StopWatch
    {
        if (isset($this->watches[$name])) {
            $this->watches[$name]['end'] = microtime(true);
            $this->watches[$name]['duration'] = $this->watches[$name]['end'] - $this->watches[$name]['start'];

            if ($log) {
                $dataStr = count($data) ? ' | Data: (' . print_r($data, 1) . ')' : '';
                Yii::debug(sprintf('STOPWATCH: %s `%s` took %f sec. to execute%s', str_repeat('-', $this->running), $name, $this->getDuration($name), $dataStr), 'profiling');
            }

            $this->running--;

        } else {
            Yii::warning("StopWatch `{$name}` was never started.");
        }

        return $this;
    }

    /**
     * Write stopwatch start point to log
     *
     * @param string $name
     * @param array  $data
     * @return StopWatch
     * @deprecated
     */
    public function logStart(string $name, array $data = []): StopWatch
    {
        $dataStr = count($data) ? ' | Data: (' . print_r($data, 1) . ')' : '';
        Yii::debug(sprintf('STOPWATCH: %s `%s` started%s', str_repeat('-', $this->running), $name, $dataStr), 'profiling');
        return $this;
    }

    /**
     * Write stopwatch result to log
     *
     * @param string $name
     * @param array  $data
     * @return StopWatch
     * @deprecated
     */
    public function logEnd(string $name, array $data = []): StopWatch
    {
        $dataStr = count($data) ? ' | Data: (' . print_r($data, 1) . ')' : '';
        Yii::debug(sprintf('STOPWATCH: %s `%s` took %f sec. to execute%s', str_repeat('-', $this->running), $name, $this->getDuration($name), $dataStr), 'profiling');
        return $this;
    }

    /**
     * Get duration by stopwatch name
     *
     * @param string $name
     * @return float|null
     */
    public function getDuration(string $name): ?float
    {
        if (isset($this->watches[$name]) && isset($this->watches[$name]['duration'])) {
            return $this->watches[$name]['duration'];
        }

        return null;
    }

}
