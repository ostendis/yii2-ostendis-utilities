<?php

namespace Ostendis\Utilities\models\enum;

use yii\base\Model;

/**
 * Class MsSqlData
 * Collection of MSSQL constants
 *
 * @package   Ostendis\Utilities\models\enum
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class MsSqlData extends Model
{
    const TINYINT_MIN = 0;
    const TINYINT_MAX = 255;

    const SMALLINT_MIN = -32768;
    const SMALLINT_MAX = 32767;

    const INT_MIN = -2147483648;
    const INT_MAX = 2147483647;

    /**
     * SQL datetime formats
     * Date/Time format strings to emulate SQL formats in PHP
     *
     * @see https://docs.microsoft.com/en-us/sql/t-sql/functions/date-and-time-data-types-and-functions-transact-sql?view=sql-server-2017
     */
    const FORMAT_DATETIME2 = 'Y-m-d H:i:s.u';   // YYYY-MM-DD hh:mm:ss[.nnnnnnn]
    const FORMAT_DATETIME = 'Y-m-d H:i:s.v';    // YYYY-MM-DD hh:mm:ss[.nnn]
    const FORMAT_SMALLDATETIME = 'Y-m-d H:i:s'; // YYYY-MM-DD hh:mm:ss
    const FORMAT_DATE = 'Y-m-d';                // YYYY-MM-DD
    const FORMAT_TIME = 'H:i:s.u';              // hh:mm:ss[.nnnnnnn]
    const FORMAT_SMALLTIME = 'H:i:s';           // hh:mm:ss

}
