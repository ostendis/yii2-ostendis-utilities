<?php

namespace Ostendis\Utilities\models;

use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\db\Connection;

/**
 * ActiveRecord extends the existing AR class with the ability to set other DB connection
 *
 * @package   Ostendis\Utilities\models
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class ActiveRecord extends \yii\db\ActiveRecord
{

    /** @var Connection */
    public static $db = null;

    /**
     * {@inheritdoc}
     * @return Connection|object
     */
    public static function getDb()
    {
        if (self::$db !== null && self::$db instanceof Connection) {
            return self::$db;
        }

        return Yii::$app->getDb();
    }

    /**
     * Explicitly set a different DB Connection for a model class
     *
     * @param $connection
     * @throws InvalidConfigException
     */
    public static function setDb($connection): void
    {
        if ($connection instanceof Connection) {
            self::$db = $connection;

        } else if (is_string($connection) && Yii::$app->has($connection) && Yii::$app->get($connection) instanceof Connection) {
            self::$db = Yii::$app->get($connection);

        } else {
            throw new InvalidArgumentException('Given argument `$connection` must be of type `string` or `\yii\db\Connection`');
        }
    }
}
