<?php

namespace Ostendis\Utilities\validators;

use yii\validators\Validator;

/**
 * Class ArrayValidator
 *
 * @package   Ostendis\Utilities\validators
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class ArrayValidator extends Validator
{
    /**
     * Checks, if $attribute is an array
     *
     * @param \yii\base\Model $model
     * @param string          $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!is_array($model->$attribute)) {
            $this->addError($model, $attribute, 'The format of {attribute} is invalid (not array).');
        }
    }
}
