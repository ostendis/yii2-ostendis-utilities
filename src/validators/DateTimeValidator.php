<?php

namespace Ostendis\Utilities\validators;

use yii\validators\Validator;

/**
 * Class DateTimeValidator
 *
 * @package   Ostendis\Utilities\validators
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class DateTimeValidator extends Validator
{
    /**
     * Checks, if $attribute is an instance of \DateTime
     *
     * @param $model
     * @param $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!$model->$attribute instanceof \DateTime) {
            $this->addError($model, $attribute, 'The format of {attribute} is invalid (not DateTime).');
        }
    }
}
