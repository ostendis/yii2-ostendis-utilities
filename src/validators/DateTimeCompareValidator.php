<?php

namespace Ostendis\Utilities\validators;

use DateTime;
use yii\base\InvalidConfigException;
use yii\validators\CompareValidator;

/**
 * Class DateTimeCompareValidator
 *
 * @package   Ostendis\Utilities\validators
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class DateTimeCompareValidator extends CompareValidator
{
    /**
     * {@inheritdoc}
     * @param DateTime $value
     * @return array|null
     * @throws \yii\base\InvalidConfigException
     */
    protected function validateValue($value)
    {
        if (!is_int($this->compareValue) && !($this->compareValue instanceof DateTime)) {
            throw new InvalidConfigException('DateTimeCompareValidator::compareValue must be either `DateTime` or `integer`');
        }

        $compare = $this->compareValue instanceof DateTime ? $this->compareValue->getTimestamp() : $this->compareValue;

        if (!parent::compareValues($this->operator, self::TYPE_NUMBER, $value->getTimestamp(), $compare)) {
            return [$this->message, [
                'compareAttribute'        => $this->compareValue,
                'compareValue'            => is_int($this->compareValue) ? $this->compareValue : $this->compareValue->getTimestamp(),
                'compareValueOrAttribute' => is_int($this->compareValue) ? $this->compareValue : $this->compareValue->getTimestamp(),
            ]];
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAttribute($model, $attribute)
    {
        $result = $this->validateValue($model->$attribute);
        if (!empty($result)) {
            $this->addError($model, $attribute, $result[0], $result[1]);
        }
    }
}
