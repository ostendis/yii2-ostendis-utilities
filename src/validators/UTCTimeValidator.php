<?php

namespace Ostendis\Utilities\validators;

use yii\validators\Validator;

/**
 * Class UTCTimeValidator
 *
 * @package   Ostendis\Utilities\validators
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class UTCTimeValidator extends Validator
{
    const UTC_PATTERN = '/^\d{4}(-\d{2}){2}T\d{2}(:\d{2}){2}\.\d{3}Z$/';

    protected $message = 'Value must be a string in the UTC format {format}';
    protected $format = 'yyyy-mm-ddThh:mm:ss.vvvZ';

    /**
     * {@inheritdoc}
     */
    public function validateValue($value)
    {
        $valid = preg_match(static::UTC_PATTERN, $value) === 1;
        if (!$valid) {
            return [$this->message, [
                'format' => $this->format,
            ]];
        }

        return null;
    }
}
