<?php
$params = require __DIR__ . '/test_params.php';

/**
 * Application configuration shared by all test types
 */
return [
    'id'         => 'yii2-ostendis-utilities',
    'basePath'   => dirname(__DIR__),
    'language'   => 'de',
    'timeZone'   => 'Europe/Zurich',
    'components' => [
        'formatter' => [
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'dateFormat'     => 'php:Y-m-d',
            'timeFormat'     => 'php:H:i:s',
        ],
    ],

    'modules' => [
    ],

    'params' => $params,
];
